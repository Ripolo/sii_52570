// Alvaro Rebollo Berlana

// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ClientMundo::ClientMundo()
{
	Init();
}

ClientMundo::~ClientMundo()
{
	//write(fd,"f",1);
	//close(fd);
	close(fdcom);
	unlink("Server_Client");
	close(fdcom2);
	unlink("Client_Server");
}

void ClientMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void ClientMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void ClientMundo::OnTimer(int value)
{	
	char cad2[200];

//ACTUALIZACIÓN DE LAS POSICIONES DEL MUNDO(FIFO)

	read(fdcom,cad2,sizeof(char)*200);
	sscanf(cad2,"%f %f %f %f %f %f %f %f %f %f %f %d %d",&esfera.radio,&esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);
	
//ACTUALIZACIÓN DE LAS COORDENADAS DEL BOT(MEMORIA COMPARTIDA)

	d->esfera=esfera;
	d->raqueta1=jugador1;

	switch(d->accion)
	{
	
	case 1: OnKeyboardDown('w');break;
	case -1: OnKeyboardDown('s');break;
	default: break;
	
	}

}

void ClientMundo::OnKeyboardDown(unsigned char key, int x, int y)
{

	// ENVIO DE TECLAS

	char cad2[200];
	int num2;

	num2=sprintf(cad2,"%c", key);
	write(fdcom2,cad2,num2);
/*
	switch(key)
	{
		
	//case 'a':jugador1.velocidad.x=-1;break;
	//case 'd':jugador1.velocidad.x=1;break;
	case 's':
		jugador1.velocidad.y=-4;break;
	case 'w':
		jugador1.velocidad.y=4;break;
	case 'l':
		jugador2.velocidad.y=-4;break;
	case 'o':
		jugador2.velocidad.y=4;break;
	
	}
*/

}

void ClientMundo::Init()
{
	int fd2;
	DatosMemCompartida data;
	
	// CREACIÓN DEL PRIMER ENLACE (ENVIO DE COORDENADAS)

	mkfifo("Server_Client",0777);
	fdcom=open("Server_Client",O_RDONLY);

	// CREACIÓN DEL SEGUNDO ENLACE (RECEPCIÓN DE TECLAS)

	mkfifo("Client_Server",0777);
	fdcom2=open("Client_Server",O_WRONLY);
	
	//fd=open("tub", O_WRONLY);
	fd2=open("datos.txt",O_RDWR|O_CREAT,0777);
	write(fd2,&data,sizeof(DatosMemCompartida));
	d=(DatosMemCompartida*)mmap(NULL,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,fd2,0);
	close(fd2);

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
