#include "DatosMemCompartida.h"
#include <sys/mman.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>

int main(int argc, char * argv[])
{
int fd;
DatosMemCompartida* d;

fd=open("datos.txt",O_RDWR,0777);
d=(DatosMemCompartida*)mmap(NULL,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
close(fd);

while(1)
{
	if(((d->raqueta1.y2+d->raqueta1.y1)/2)<(d->esfera.centro.y))
	{
		d->accion=1;
	}
	else 
	{
		if(((d->raqueta1.y2+d->raqueta1.y1)/2)>(d->esfera.centro.y))
		{
			d->accion=-1;
		}
		else
		{
			d->accion=0;
		}
	}
	usleep(25000);
}
return 0;
}
