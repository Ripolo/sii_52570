// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include <pthread.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
//#include "Raqueta.h"
#include "DatosMemCompartida.h"

class ServerMundo  
{
public:
	void Init();
	ServerMundo();
	virtual ~ServerMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x=0, int y=0);
	void OnTimer(int value);
	void OnDraw();	
	void RecibeComandos();

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	//DatosMemCompartida* d;

	int puntos1;
	int puntos2;

	int fd;
	int fdcom;
	int fdcom2;

	pthread_t thid;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
